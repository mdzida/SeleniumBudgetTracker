package com.github.mdzida.selenium.budget.tracker.components;

import com.github.mdzida.selenium.budget.tracker.pages.MyAccountsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.github.mdzida.selenium.budget.tracker.pages.MyAccountsPage.ACCOUNT_TYPE_SELECT;
import static com.github.mdzida.selenium.budget.tracker.pages.MyAccountsPage.SUCCESSFULLY_ADDED_NEW_ACCOUNT;

public class AddAccountProvider {
    private static final int TIME_TO_WAIT = 2;
    private static final String ACCOUNT_NAME = "First Account";
    private static final String AMOUNT_OF_MONEY = "2000";
    private static final String CHECKBOX_CHECK_OPTION = "checking";

    private MyAccountsPage myAccountsPage;
    private WebDriverWait wait;
    private PageOperation pageOperation;

    public AddAccountProvider(WebDriver driver) {
        myAccountsPage = new MyAccountsPage(driver);
        pageOperation = new PageOperation(driver);
        wait = new WebDriverWait(driver, TIME_TO_WAIT);
    }

    public void fillAccountNameInput(String accountName, String amount) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(ACCOUNT_TYPE_SELECT));
        pageOperation.fillInput(myAccountsPage.getAccountNameInput(), accountName);
        pageOperation.fillInput(myAccountsPage.getCurrentBalanceInput(), amount);
    }

    public void waitForCreateNewAccount() {
        myAccountsPage.clickSmallAddButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(SUCCESSFULLY_ADDED_NEW_ACCOUNT));
    }

    public void addNewAccount() {
        myAccountsPage.clickAddAccountButton();

        fillAccountNameInput(ACCOUNT_NAME, AMOUNT_OF_MONEY);
        pageOperation.selectFromDropdown(myAccountsPage.dropdownSelectPath(), CHECKBOX_CHECK_OPTION);

        waitForCreateNewAccount();
    }
}