package com.github.mdzida.selenium.budget.tracker.components;

import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.AccountDashboardPage;
import com.github.mdzida.selenium.budget.tracker.pages.LandingPage;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class LoginLogoutProvider {
    private static final String USERNAME = Config.config().getProperty("username");
    private static final String PASSWORD = Config.config().getProperty("password");
    private static final int TIME_TO_WAIT = 10;

    private WebDriver driver;
    private LandingPage landingPage;
    private AccountDashboardPage accountDashboardPage;

    public LoginLogoutProvider(WebDriver driver) {
        this.driver = driver;
        landingPage = new LandingPage(driver);
        accountDashboardPage = new AccountDashboardPage(driver);
    }

    public void loginWithDefaultCredentials() {
        landingPage.openBasePage();
        landingPage.goToLoginPopup();
        landingPage.fillEmailInput(USERNAME);
        landingPage.fillPasswordInput(PASSWORD);
        landingPage.clickLoginButton();
        driver.manage().timeouts().implicitlyWait(TIME_TO_WAIT, TimeUnit.SECONDS);
    }

    public void logout() {
        accountDashboardPage.clickLogout();
    }
}