package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyIncomePage {
    private static final By INCOME_LIST = By.linkText("Income List");

    private WebDriver driver;

    public MyIncomePage(WebDriver driver){
        this.driver = driver;
    }

    public String getIncomeListText(){
        return driver.findElement(INCOME_LIST).getText();
    }
}
