package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.*;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.AccountDashboardPage;
import com.github.mdzida.selenium.budget.tracker.pages.MyBudgetPage;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AddNewBudgetTest {
    private static final String SUCCESS_MESSAGE = "Success";
    private static final String EXPENSE_FIELD = "expense";
    private static final String SUCCESS_DELETE_MESSAGE = "deleted your selected";
    private static final String ALERT_MESSAGE = "Would you also like";
    private static final String NO_CATEGORIES_CREATED_MESSAGE = "You have no categories created";
    private static final String FOOD_SELECT_OPTION = "Food";
    private static final String DINING_OUT_SELECT_OPTION = "Dining Out";
    private static final String FIRST_BUDGET_TYPE_OPTION = "0";
    private static final String AMOUNT_CASH_INPUT = "50";


    private WebDriver driver;
    private LoginLogoutProvider loginLogoutProvider;
    private MyBudgetPage myBudgetPage;
    private AccountDashboardPage accountDashboardPage;
    private PageOperation pageOperation;

    @BeforeClass
    public void setUp(){
        driver = Config.getDefaultDriver();

        loginLogoutProvider = new LoginLogoutProvider(driver);
        myBudgetPage = new MyBudgetPage(driver);
        accountDashboardPage = new AccountDashboardPage(driver);
        pageOperation = new PageOperation(driver);

        loginLogoutProvider.loginWithDefaultCredentials();

        AddAccountProvider addAccountProvider = new AddAccountProvider(driver);
        addAccountProvider.addNewAccount();
    }

    @BeforeMethod
    public void goToBudgetSubpage(){
        accountDashboardPage.clickMyBudget();
        assertThat(myBudgetPage.isAddBudgetPresent()).isTrue();
    }

    @Test
    public void addBudget(){
        addNewBudget();

        assertThat(myBudgetPage.getSuccessMessage()).contains(SUCCESS_MESSAGE);
        assertThat(myBudgetPage.getExpenseField()).isEqualTo(EXPENSE_FIELD);
    }

    @Test
    public void deleteBudgetCheckbox(){
        pageOperation.selectCheckboxAndClickButton(myBudgetPage.getDeleteCheckbox(), myBudgetPage.getButtonDelete());
        assertThat(myBudgetPage.getSuccessMessage()).contains(SUCCESS_DELETE_MESSAGE);
    }

    @Test
    public void deleteBudgetTrashPic() {
        addNewBudget();

        pageOperation.deleteWithTrashPic(myBudgetPage.getDeleteTrashPic());
        assertThatThrownBy(() -> myBudgetPage.getButtonDelete().click())
                .isInstanceOf(UnhandledAlertException.class)
                .hasMessageContaining(ALERT_MESSAGE);

        pageOperation.acceptAlert();

        assertThat(myBudgetPage.getNoCategoriesMessage()).contains(NO_CATEGORIES_CREATED_MESSAGE);
    }

    private void addNewBudget(){
        myBudgetPage.goToAddBudget();
        pageOperation.selectFromDropdown(myBudgetPage.getCategoryDropdown(), FOOD_SELECT_OPTION);
        pageOperation.selectFromDropdown(myBudgetPage.getSubCategoryDropdown(), DINING_OUT_SELECT_OPTION);
        pageOperation.fillInput(myBudgetPage.getAmountInput(), AMOUNT_CASH_INPUT);
        pageOperation.selectFromDropdown(myBudgetPage.getBudgetTypeDropdown(), FIRST_BUDGET_TYPE_OPTION);
        myBudgetPage.clickAddBudgetButton();
    }

    @AfterClass
    public void tearDown(){
        loginLogoutProvider.logout();
        driver.quit();
    }
}
