package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.LoginLogoutProvider;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SubpagesAccessibilityTest {
    private WebDriver driver;
    private AccountDashboardPage accountDashboardPage;
    private LoginLogoutProvider loginLogoutProvider;
    private OverviewPage overviewPage;
    private MyAccountsPage myAccountsPage;
    private MyBillsPage myBillsPage;
    private MyBudgetPage myBudgetPage;
    private MyIncomePage myIncomePage;
    private MyTransactionsPage myTransactionsPage;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();

        accountDashboardPage = new AccountDashboardPage(driver);
        loginLogoutProvider = new LoginLogoutProvider(driver);
        overviewPage = new OverviewPage(driver);
        myAccountsPage = new MyAccountsPage(driver);
        myBillsPage = new MyBillsPage(driver);
        myBudgetPage = new MyBudgetPage(driver);
        myIncomePage = new MyIncomePage(driver);
        myTransactionsPage = new MyTransactionsPage(driver);

        loginLogoutProvider.loginWithDefaultCredentials();
    }

    @Test
    public void goToOverviewPage() {
        accountDashboardPage.clickOverview();
        assertThat(overviewPage.getTransactionsText()).isNotBlank();
    }

    @Test
    public void goToMyAccountsPage() {
        accountDashboardPage.clickMyAccounts();
        assertThat(myAccountsPage.isCashFieldDisplayed()).isTrue();
    }

    @Test
    public void goToMyBillsPage() {
        accountDashboardPage.clickMyBills();
        assertThat(myBillsPage.isOverduePresent()).isTrue();
    }

    @Test
    public void goToMyIncomePage() {
        accountDashboardPage.clickMyIncome();
        assertThat(myIncomePage.getIncomeListText()).isNotBlank();
    }

    @Test
    public void goToMyBudgetPage() {
        accountDashboardPage.clickMyBudget();
        assertThat(myBudgetPage.isAddBudgetPresent()).isTrue();
    }

    @Test
    public void goToMyTransactionsPage() {
        accountDashboardPage.clickMyTransactions();
        assertThat(myTransactionsPage.isAddTransactionPresent()).isTrue();
    }

    @AfterClass
    public void tearDown() {
        loginLogoutProvider.logout();
        driver.quit();
    }
}