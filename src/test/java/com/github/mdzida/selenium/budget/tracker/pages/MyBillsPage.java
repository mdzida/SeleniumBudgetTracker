package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyBillsPage {
    private WebDriver driver;
    private static final By OVERDUE_BILLS = By.id("submoverdueId");

    public MyBillsPage(WebDriver driver){
        this.driver = driver;
    }

    public boolean isOverduePresent(){
        return driver.findElement(OVERDUE_BILLS).isDisplayed();
    }
}
