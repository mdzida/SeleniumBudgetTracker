package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.AddAccountProvider;
import com.github.mdzida.selenium.budget.tracker.components.LoginLogoutProvider;
import com.github.mdzida.selenium.budget.tracker.components.PageOperation;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.MyAccountsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AddNewAccountTest {
    private static final String AMOUNT = "2000";
    private static final String CHECKING = "checking";
    private static final String ACCOUNT_NAME = "First Account";
    private static final String SUCCESS_MESSAGE = "Success!";

    private WebDriver driver;
    private LoginLogoutProvider loginLogoutProvider;
    private MyAccountsPage myAccountsPage;
    private AddAccountProvider addAccountProvider;
    private PageOperation pageOperation;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();

        loginLogoutProvider = new LoginLogoutProvider(driver);
        myAccountsPage = new MyAccountsPage(driver);
        addAccountProvider = new AddAccountProvider(driver);
        pageOperation = new PageOperation(driver);

        loginLogoutProvider.loginWithDefaultCredentials();
    }

    @Test
    public void addNewAccount() {
        assertThat(myAccountsPage.isCashFieldDisplayed()).isTrue();

        myAccountsPage.clickAddAccountButton();
        addAccountProvider.fillAccountNameInput(ACCOUNT_NAME, AMOUNT);
        pageOperation.selectFromDropdown(myAccountsPage.dropdownSelectPath(), CHECKING);

        addAccountProvider.waitForCreateNewAccount();
        assertThat(myAccountsPage.getSuccessfullyAddedNewAccountMessage()).startsWith(SUCCESS_MESSAGE);
        assertThat(myAccountsPage.isAccountCreated()).isTrue();
        pageOperation.selectCheckboxAndClickButton(myAccountsPage.getDeleteCheckbox(), myAccountsPage.getDeleteButton());
        pageOperation.acceptAlert();
    }

    @Test
    public void deleteAccountWithButton() {
        addAccountProvider.addNewAccount();
        pageOperation.selectCheckboxAndClickButton(myAccountsPage.getDeleteCheckbox(), myAccountsPage.getDeleteButton());
        pageOperation.acceptAlert();
        checkAccountSuccessfullyDeleted();
    }

    @Test
    public void deleteAccountWithTrashPic() {
        addAccountProvider.addNewAccount();
        pageOperation.deleteWithTrashPic(myAccountsPage.getDeleteTrashPic());
        pageOperation.acceptAlert();
        checkAccountSuccessfullyDeleted();
    }

    private void checkAccountSuccessfullyDeleted() {
        assertThat(myAccountsPage.isAccountCreated()).isFalse();
    }

    @AfterClass
    public void tearDown() {
        loginLogoutProvider.logout();
        driver.quit();
    }
}
