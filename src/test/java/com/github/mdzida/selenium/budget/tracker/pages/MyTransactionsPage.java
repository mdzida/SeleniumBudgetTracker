package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyTransactionsPage {
    private static final By ADD_TRANSACTION = By.linkText("Add a Transaction");
    private static final By ENTER_TRANSACTION = By.xpath("//*[@id=\"showhide\"]/tbody/tr[10]/td/input[1]");
    private static final By PAYEE = By.xpath("//*[@id=\"showhide\"]/tbody/tr[1]/td[2]/div[1]/input[2]");
    private static final By STATUS = By.xpath("//*[@id=\"showhide\"]/tbody/tr[3]/td[2]/select");
    private static final By AMOUNT = By.xpath("//*[@id=\"showhide\"]/tbody/tr[1]/td[4]/input");
    private static final By ACCOUNT = By.id("accountId");
    private static final By TRANSACTION_ROW = By.xpath("//*[@class=\"transactionRow\"]/td[1]");
    private static final By DELETE_TRANSACTION_BUTTON = By.className("redbutton");
    private static final By DELETE_TRANSACTION_CHECKBOX = By.xpath("//*[@class=\"submenu1_noright submenu_left\"]/input");
    private static final By DELETE_TRANSACTION_TRASH_PIC = By.cssSelector("img[src=\"pics/icons/trash2.png\"");
    private static final By BLOCK_DELETE_BUTTON = By.xpath("/html/body/div[12]/div[3]/div/button[1]");
//    /html/body/div[12]/div[3]/div/button[1]

    private WebDriver driver;

    public MyTransactionsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isTransactionRowPresent() {
        return driver.findElements(TRANSACTION_ROW).size() > 0;
    }

    public boolean isAddTransactionPresent() {
        return driver.findElement(ADD_TRANSACTION).isDisplayed();
    }

    public void goToAddNewTransaction() {
        driver.findElement(ADD_TRANSACTION).click();
    }

    public void clickEnterTransactionButton() {
        driver.findElement(ENTER_TRANSACTION).click();
    }

    public WebElement getPeyeeInput() {
        return driver.findElement(PAYEE);
    }

    public WebElement getStatusDropdown() {
        return driver.findElement(STATUS);
    }

    public WebElement getAmountInput() {
        return driver.findElement(AMOUNT);
    }

    public WebElement getAccountDropdown(){
        return driver.findElement(ACCOUNT);
    }

    public WebElement getDeleteTranButton() {
        return driver.findElement(DELETE_TRANSACTION_BUTTON);
    }

    public WebElement getDeleteCheckbox() {
        return driver.findElement(DELETE_TRANSACTION_CHECKBOX);
    }

    public WebElement getDeleteTrashPic() {
        return driver.findElement(DELETE_TRANSACTION_TRASH_PIC);
    }

    public void clickBlockButtonDelete() {
        driver.findElement(BLOCK_DELETE_BUTTON).click();
    }
}
