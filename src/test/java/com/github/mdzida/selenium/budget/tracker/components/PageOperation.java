package com.github.mdzida.selenium.budget.tracker.components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageOperation {
    private static final int DEFAULT_WAIT_TIME = 10;

    private WebDriver driver;
    private WebDriverWait webDriverWait;

    public PageOperation(WebDriver driver){
        this.driver = driver;
        this.webDriverWait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
    }

    public void fillInput(WebElement element, String data) {
        element.click();
        element.clear();
        element.sendKeys(data);
    }

    public void selectFromDropdown(WebElement dropdownPath, String name) {
        Select select = new Select(dropdownPath);
        select.selectByValue(name);
    }

    public void selectCheckboxAndClickButton(WebElement checkbox, WebElement button) {
        checkbox.click();
        button.click();
    }

    public void deleteWithTrashPic(WebElement trashPic) {
        new WebDriverWait(driver, DEFAULT_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(trashPic));
        trashPic.click();
    }

    public void acceptAlert() {
        new WebDriverWait(driver, DEFAULT_WAIT_TIME).until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
    }
}

