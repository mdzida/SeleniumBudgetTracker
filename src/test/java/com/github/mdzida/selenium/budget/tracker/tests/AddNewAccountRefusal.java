package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.AddAccountProvider;
import com.github.mdzida.selenium.budget.tracker.components.LoginLogoutProvider;
import com.github.mdzida.selenium.budget.tracker.components.PageOperation;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.AccountDashboardPage;
import com.github.mdzida.selenium.budget.tracker.pages.MyAccountsPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

public class AddNewAccountRefusal {
    private static final String AMOUNT = "2000";
    private static final String EMPTY_AMOUNT = "";
    private static final String ACCOUNT_NAME = "First Account";
    private static final String EMPTY_ACCOUNT_NAME = "";
    private static final String CHECKBOX_OPTION = "checking";

    private WebDriver driver;
    private AccountDashboardPage accountDashboardPage;
    private MyAccountsPage myAccountsPage;
    private AddAccountProvider addAccountProvider;
    private PageOperation pageOperation;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();

        accountDashboardPage = new AccountDashboardPage(driver);
        myAccountsPage = new MyAccountsPage(driver);
        addAccountProvider = new AddAccountProvider(driver);
        pageOperation = new PageOperation(driver);

        LoginLogoutProvider loginLogoutProvider = new LoginLogoutProvider(driver);
        loginLogoutProvider.loginWithDefaultCredentials();

    }

    @Test
    public void addAccountWithoutName() {
        myAccountsPage.clickAddAccountButton();
        addAccountProvider.fillAccountNameInput(EMPTY_ACCOUNT_NAME, AMOUNT);
        pageOperation.selectFromDropdown(myAccountsPage.dropdownSelectPath(), CHECKBOX_OPTION);

        clickAddAccountButtonAndAcceptAlert();
    }

    @Test
    public void addAccountWithoutType() {
        accountDashboardPage.clickMyAccounts();
        myAccountsPage.clickAddAccountButton();
        addAccountProvider.fillAccountNameInput(ACCOUNT_NAME, AMOUNT);

        clickAddAccountButtonAndAcceptAlert();
    }

    @Test //Bug encountered - possibility of add new account without entering current balance
    public void addAccountWithoutCurrentBalance() {
        myAccountsPage.clickAddAccountButton();
        addAccountProvider.fillAccountNameInput(ACCOUNT_NAME, EMPTY_AMOUNT);
        pageOperation.selectFromDropdown(myAccountsPage.dropdownSelectPath(), CHECKBOX_OPTION);

        myAccountsPage.clickSmallAddButton();
    }

    private void clickAddAccountButtonAndAcceptAlert() {
        myAccountsPage.clickSmallAddButton();
        pageOperation.acceptAlert();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
