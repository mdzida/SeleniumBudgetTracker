package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyBudgetPage {
    private static final By ADD_BUDGET = By.linkText("Add to Budget");
    private static final By DROPDOWN_CATEGORY = By.xpath("//*[@id=\"budget_div\"]/div/form/table/tbody/tr[2]/td[2]/select");
    private static final By AMOUNT_INPUT = By.cssSelector("#AmountId");
    private static final By DROPDOWN_SUB_CAT = By.xpath("//*[@id=\"budget_div\"]/div/form/table/tbody/tr[3]/td[2]/select");
    private static final By DROPDOWN_BUDGET_TYPE = By.id("incomeexpenseid");
    private static final By ADD_BUDGET_BUTTON = By.name("addbudgetbtn");
    private static final By SUCCESS_FIELD = By.className("successmsg");
    private static final By NO_CAT_CREATED = By.xpath("//*[@id=\"budgetmain\"]/table/tbody/tr[2]/td/p");
    private static final By EXPENSE_FIELD = By.className("redTxt");
    private static final By DELETE_CHECKBOX = By.xpath("//*[@class=\"expensebottom apps_datarows_left apps_datarows1_noright\"]/input");
    private static final By DELETE_TRASH_PIC = By.cssSelector("img[src=\"pics/icons/trash2.png\"");
    private static final By DELETE_BUTTON = By.className("redButton");

    private WebDriver driver;

    public MyBudgetPage(WebDriver driver){
        this.driver = driver;
    }

    public boolean isAddBudgetPresent(){
        return driver.findElement(ADD_BUDGET).isDisplayed();
    }

    public void goToAddBudget(){
        driver.findElement(ADD_BUDGET).click();
    }

    public WebElement getCategoryDropdown(){
        return driver.findElement(DROPDOWN_CATEGORY);
    }

    public WebElement getSubCategoryDropdown(){
        return driver.findElement(DROPDOWN_SUB_CAT);
    }

    public WebElement getBudgetTypeDropdown(){
        return driver.findElement(DROPDOWN_BUDGET_TYPE);
    }

    public WebElement getAmountInput(){
        return driver.findElement(AMOUNT_INPUT);
    }

    public void clickAddBudgetButton(){
        driver.findElement(ADD_BUDGET_BUTTON).click();
    }

    public String getSuccessMessage(){
        return driver.findElement(SUCCESS_FIELD).getText();
    }

    public String getNoCategoriesMessage(){
        return driver.findElement(NO_CAT_CREATED).getText();
    }

    public String getExpenseField(){
        return driver.findElement(EXPENSE_FIELD).getText();
    }

    public WebElement getDeleteCheckbox() {
        return driver.findElement(DELETE_CHECKBOX);
    }

    public WebElement getDeleteTrashPic() {
        return driver.findElement(DELETE_TRASH_PIC);
    }

    public WebElement getButtonDelete() {
        return driver.findElement(DELETE_BUTTON);
    }
}
