package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.LoginLogoutProvider;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.AccountDashboardPage;
import com.github.mdzida.selenium.budget.tracker.pages.LandingPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginLogoutTest {
    private WebDriver driver;
    private LandingPage landingPage;
    private LoginLogoutProvider loginLogoutProvider;
    private AccountDashboardPage accountDashboardPage;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();
        landingPage = new LandingPage(driver);
        loginLogoutProvider = new LoginLogoutProvider(driver);
        accountDashboardPage = new AccountDashboardPage(driver);
    }

    @Test
    public void login() {
        loginLogoutProvider.loginWithDefaultCredentials();
        assertThat(accountDashboardPage.getNewAccount()).isNotBlank();
    }

    @Test
    public void logout() {
        loginLogoutProvider.logout();
        assertThat(landingPage.isLoginPopupEnabled()).isTrue();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}