package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyAccountsPage {
    private static final By CASH = By.linkText("Cash");
    private static final By ADD_ACCOUNT_BUTTON = By.linkText("New Account");
    private static final By ACCOUNT_NAME_INPUT = By.xpath("//*[@id=\"accountNameId\"]");
    private static final By CURRENT_BALANCE_INPUT = By.name("openingbalance");
    private static final By ADD_SMALL_BUTTON = By.xpath("//*[@id=\"newaccountID2\"]/tbody/tr[9]/td/input[1]");
    private static final By DELETE_ACCOUNT_BUTTON = By.className("redButton");
    private static final By CREATED_ACCOUNT_FIELD = By.linkText("First Account");
    private static final By DELETE_ACCOUNT_CHECKBOX = By.name("account_1");
    private static final By DELETE_ACCOUNT_TRASH_PIC = By.cssSelector("img[src=\"pics/icons/trash2.png\"]");

    public static final By ACCOUNT_TYPE_SELECT = By.name("accttype");
    public static final By SUCCESSFULLY_ADDED_NEW_ACCOUNT = By.id("accountupdate_div");

    private WebDriver driver;

    public MyAccountsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isAccountCreated(){
        return driver.findElements(CREATED_ACCOUNT_FIELD).size() > 0;
    }

    public String getSuccessfullyAddedNewAccountMessage() {
        return driver.findElement(SUCCESSFULLY_ADDED_NEW_ACCOUNT).getText();
    }

    public void clickSmallAddButton() {
        driver.findElement(ADD_SMALL_BUTTON).click();
    }

    public WebElement getAccountNameInput() {
        return driver.findElement(ACCOUNT_NAME_INPUT);
    }

    public WebElement getCurrentBalanceInput() {
        return driver.findElement(CURRENT_BALANCE_INPUT);
    }

    public WebElement dropdownSelectPath() {
        return driver.findElement(ACCOUNT_TYPE_SELECT);
    }

    public boolean isCashFieldDisplayed() {
        return driver.findElement(CASH).isDisplayed();
    }

    public void clickAddAccountButton() {
        driver.findElement(ADD_ACCOUNT_BUTTON).click();
    }

    public WebElement getDeleteButton() {
        return driver.findElement(DELETE_ACCOUNT_BUTTON);
    }

    public WebElement getDeleteCheckbox() {
        return driver.findElement(DELETE_ACCOUNT_CHECKBOX);
    }

    public WebElement getDeleteTrashPic() {
        return driver.findElement(DELETE_ACCOUNT_TRASH_PIC);
    }
}
