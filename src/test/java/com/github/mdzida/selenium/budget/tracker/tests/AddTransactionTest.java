package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.*;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.AccountDashboardPage;
import com.github.mdzida.selenium.budget.tracker.pages.MyTransactionsPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AddTransactionTest {
    private static final String FIRST_USER = "First User";
    private static final String DROPDOWN_VALUE = "2";
    private static final String AMOUNT_OF_MONEY = "2000";
    private static final String ACCOUNT_NAME = "cash";

    private WebDriver driver;
    private LoginLogoutProvider loginLogoutProvider;
    private MyTransactionsPage myTransactionsPage;
    private AccountDashboardPage accountDashboardPage;
    private AddTransactionProvider addTransactionProvider;
    private PageOperation pageOperation;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();

        myTransactionsPage = new MyTransactionsPage(driver);
        accountDashboardPage = new AccountDashboardPage(driver);
        addTransactionProvider = new AddTransactionProvider(driver);
        pageOperation = new PageOperation(driver);

        loginLogoutProvider = new LoginLogoutProvider(driver);
        loginLogoutProvider.loginWithDefaultCredentials();

        AddAccountProvider addAccountProvider = new AddAccountProvider(driver);
        addAccountProvider.addNewAccount();
    }

    @Test
    public void addNewTransaction() {
        accountDashboardPage.clickMyTransactions();
        assertThat(myTransactionsPage.isAddTransactionPresent()).isTrue();

        addTransactionProvider.addNewTransaction(FIRST_USER, DROPDOWN_VALUE, AMOUNT_OF_MONEY, ACCOUNT_NAME);
        assertThat(myTransactionsPage.isTransactionRowPresent()).isTrue();
    }

    @Test
    public void rmByCheckbox() {
        accountDashboardPage.clickMyTransactions();
        addTransactionProvider.addNewTransaction(FIRST_USER, DROPDOWN_VALUE, AMOUNT_OF_MONEY, ACCOUNT_NAME);

        pageOperation.selectCheckboxAndClickButton(myTransactionsPage.getDeleteCheckbox(), myTransactionsPage.getDeleteTranButton());
        pageOperation.acceptAlert();

        checkTransactionDeleted();
    }

    @Test
    public void rmByTrashPic() {
        accountDashboardPage.clickMyTransactions();

        addTransactionProvider.addNewTransaction(FIRST_USER, DROPDOWN_VALUE, AMOUNT_OF_MONEY, ACCOUNT_NAME);
        pageOperation.deleteWithTrashPic(myTransactionsPage.getDeleteTrashPic());
        myTransactionsPage.clickBlockButtonDelete();
        driver.navigate().refresh();

        checkTransactionDeleted();
    }

    private void checkTransactionDeleted() {
        assertThat(myTransactionsPage.isTransactionRowPresent()).isFalse();
    }

    @AfterClass
    public void tearDown() {
        loginLogoutProvider.logout();
        driver.quit();
    }
}