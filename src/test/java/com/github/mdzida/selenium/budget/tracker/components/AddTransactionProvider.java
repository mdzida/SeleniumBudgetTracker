package com.github.mdzida.selenium.budget.tracker.components;

import com.github.mdzida.selenium.budget.tracker.pages.MyTransactionsPage;
import org.openqa.selenium.WebDriver;

public class AddTransactionProvider {
    private MyTransactionsPage myTransactionsPage;
    private PageOperation pageOperation;

    public AddTransactionProvider(WebDriver driver) {
        myTransactionsPage = new MyTransactionsPage(driver);
        pageOperation = new PageOperation(driver);
    }

    public void addNewTransaction(String username, String dropdownStatus, String amount, String dropdownAccount) {
        myTransactionsPage.goToAddNewTransaction();
        pageOperation.fillInput(myTransactionsPage.getPeyeeInput(), username);
        pageOperation.selectFromDropdown(myTransactionsPage.getStatusDropdown(), dropdownStatus);
        pageOperation.fillInput(myTransactionsPage.getAmountInput(), amount);
        pageOperation.selectFromDropdown(myTransactionsPage.getAccountDropdown(), dropdownAccount);
        myTransactionsPage.clickEnterTransactionButton();
    }
}
