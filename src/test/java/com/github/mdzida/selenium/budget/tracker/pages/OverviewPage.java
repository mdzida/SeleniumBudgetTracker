package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OverviewPage {
    private static final By LAST_7_DAYS = By.partialLinkText("Last 7");

    private WebDriver driver;

    public OverviewPage(WebDriver driver){
        this.driver = driver;
    }

    public String getTransactionsText(){
        return driver.findElement(LAST_7_DAYS).getText();
    }
}
