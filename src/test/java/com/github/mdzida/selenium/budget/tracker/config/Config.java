package com.github.mdzida.selenium.budget.tracker.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Config {
    private static final String PROPERTIES_FILE_PATH = "application.properties";
    private static Config instance;

    private Configuration propertiesHolder;

    private Config() {
    }

    public static Config config(){
        if (instance == null) {
            instance = new Config();
            instance.loadPropertiesFromFile();
            return instance;
        }
        return instance;
    }

    private void loadPropertiesFromFile() {
        try {
            this.propertiesHolder = new PropertiesConfiguration(PROPERTIES_FILE_PATH);
        } catch (ConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException(String.format("Failed to load properties file from: %s", PROPERTIES_FILE_PATH));
        }
    }

    public String getProperty(String key) {
        return propertiesHolder.getString(key);
    }

    public static WebDriver getDefaultDriver() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }
}
