package com.github.mdzida.selenium.budget.tracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountDashboardPage {
    private static final By NEW_ACCOUNT = By.linkText("New Account");
    private static final By LOGOUT_BUTTON = By.linkText("Logout");
    private static final By OVERVIEW = By.xpath("//*[@id=\"sidelinksId\"]/div[1]");
    private static final By MY_ACCOUNTS = By.xpath("//*[@id=\"sidelinksId\"]/div[2]");
    private static final By MY_BILLS = By.xpath("//*[@id=\"sidelinksId\"]/div[3]");
    private static final By MY_INCOME = By.xpath("//*[@id=\"sidelinksId\"]/div[4]");
    private static final By MY_BUDGET = By.xpath("//*[@id=\"sidelinksId\"]/div[5]");
    private static final By MY_TRANSACTIONS= By.xpath("//*[@id=\"sidelinksId\"]/div[6]");

    private WebDriver driver;

    public AccountDashboardPage(WebDriver driver){
        this.driver = driver;
    }

    public void clickOverview(){
        driver.findElement(OVERVIEW).click();
    }

    public void clickMyAccounts(){
        driver.findElement(MY_ACCOUNTS).click();
    }

    public void clickMyBills(){
        driver.findElement(MY_BILLS).click();
    }

    public void clickMyIncome(){
        driver.findElement(MY_INCOME).click();
    }

    public void clickMyBudget(){
        driver.findElement(MY_BUDGET).click();
    }

    public void clickMyTransactions(){
        driver.findElement(MY_TRANSACTIONS).click();
    }

    public String getNewAccount(){
        return driver.findElement(NEW_ACCOUNT).getText();
    }

    public void clickLogout(){
        driver.findElement(LOGOUT_BUTTON).click();
    }
}
