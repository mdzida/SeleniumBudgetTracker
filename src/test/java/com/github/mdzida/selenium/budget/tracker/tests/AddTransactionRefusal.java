package com.github.mdzida.selenium.budget.tracker.tests;

import com.github.mdzida.selenium.budget.tracker.components.AddTransactionProvider;
import com.github.mdzida.selenium.budget.tracker.components.LoginLogoutProvider;
import com.github.mdzida.selenium.budget.tracker.components.PageOperation;
import com.github.mdzida.selenium.budget.tracker.config.Config;
import com.github.mdzida.selenium.budget.tracker.pages.AccountDashboardPage;
import com.github.mdzida.selenium.budget.tracker.pages.MyTransactionsPage;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AddTransactionRefusal {
    private static final String AMOUNT = "2000";
    private static final String EMPTY_AMOUNT = "";
    private static final String USER_NAME = "First Account";
    private static final String EMPTY_USER_NAME = "";
    private static final String DROPDOWN_STATUS = "3";
    private static final String DROPDOWN_ACCOUNT = "cash";
    private static final String EMPTY_DROPDOWN_ACCOUNT = "";
    private static final String ALERT_MESSAGE = "Please";

    private WebDriver driver;
    private LoginLogoutProvider loginLogoutProvider;
    private AddTransactionProvider addTransactionProvider;
    private AccountDashboardPage accountDashboardPage;
    private MyTransactionsPage myTransactionsPage;
    private PageOperation pageOperation;

    @BeforeClass
    public void setUp() {
        driver = Config.getDefaultDriver();

        loginLogoutProvider = new LoginLogoutProvider(driver);
        addTransactionProvider = new AddTransactionProvider(driver);
        accountDashboardPage = new AccountDashboardPage(driver);
        myTransactionsPage = new MyTransactionsPage(driver);
        pageOperation = new PageOperation(driver);

        loginLogoutProvider.loginWithDefaultCredentials();
    }

    @BeforeMethod
    private void goToMyTransactions() {
        accountDashboardPage.clickMyTransactions();
    }

    @Test
    public void addTransactionWithoutPayee() {
        addTransactionProvider.addNewTransaction(EMPTY_USER_NAME, DROPDOWN_STATUS, AMOUNT, DROPDOWN_ACCOUNT);

        enterTransactionAndHandleDeleteAlert();
    }

    @Test
    public void addTransactionWithoutAmount() {
        addTransactionProvider.addNewTransaction(USER_NAME, DROPDOWN_STATUS, EMPTY_AMOUNT, DROPDOWN_ACCOUNT);

        enterTransactionAndHandleDeleteAlert();
    }

    @Test
    public void addTransactionWithoutAccount() {
        addTransactionProvider.addNewTransaction(USER_NAME, DROPDOWN_STATUS, AMOUNT, EMPTY_DROPDOWN_ACCOUNT);

        enterTransactionAndHandleDeleteAlert();
    }

    private void enterTransactionAndHandleDeleteAlert() {
        assertThatThrownBy(() -> myTransactionsPage.clickEnterTransactionButton())
                .isInstanceOf(UnhandledAlertException.class)
                .hasMessageContaining(ALERT_MESSAGE);
        pageOperation.acceptAlert();
    }

    @AfterClass
    public void tearDown() {
        loginLogoutProvider.logout();
        driver.quit();
    }
}