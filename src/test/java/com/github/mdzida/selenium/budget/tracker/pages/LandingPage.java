package com.github.mdzida.selenium.budget.tracker.pages;

import com.github.mdzida.selenium.budget.tracker.config.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingPage {
    private static final String URL = Config.config().getProperty("baseUrl");

    private static final By GO_TO_LOGIN_BUTTON = By.id("loginbtn");
    private static final By EMAIL = By.id("emailid");
    private static final By PASSWORD = By.id("passwordid");
    private static final By LOGIN_BUTTON = By.xpath("//*[@id=\"loginformid\"]/form/div[4]/input");

    private WebDriver driver;

    public LandingPage(WebDriver driver) {
        this.driver = driver;
    }

    public void openBasePage() {
        driver.get(URL);
    }

    public boolean isLoginPopupEnabled() {
        return driver.findElement(GO_TO_LOGIN_BUTTON).isEnabled();
    }

    public void goToLoginPopup() {
        driver.findElement(GO_TO_LOGIN_BUTTON).click();
    }

    public void fillEmailInput(String email) {
        driver.findElement(EMAIL).clear();
        driver.findElement(EMAIL).sendKeys(email);
    }

    public void fillPasswordInput(String password) {
        driver.findElement(PASSWORD).clear();
        driver.findElement(PASSWORD).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(LOGIN_BUTTON).click();
    }
}