# Budget Tracker - Selenium Tests
### Description
Basic functionality of budgettracker.com page was checked. It is designed to track spending user's money. Thanks to this site one can plan their monthly budget and keep eye on all payments. Summary incomes and expenses, therefor better manage wallet. 
Attention was paid to test the main functions of the webpage:

- Login & Logout -  login with personal account created especially for these tests,
- Creation of new account - adding accounts let user to manage cash,
- Creation of transactions - follow all expenses and be aware of current state user account,
- Creation of budget - allow to better plan future expenses,
Tests against making new accounts was carried out in two versions: with valid and invalid data, in order to check if validation is properly set up. 

Technologies used: Selenium Webdriver, Java, TestNG, AssertJ.

### Requirements
In order to run tests locally one need to have installed: 

- Java Development Kit (JDK)

### How to run
The best way to run tests is by using Maven with included to the project pom.xml file. Then all what is needed, is just simply click the -run- arrow next to chosen test. 

---------
